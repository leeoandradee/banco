package br.com.itau.conta;

public class Conta {

    private double saldo;

    public Conta() {
        this.saldo = 0;
        System.out.println("Conta aberta com sucesso!");
    }

    public void getSaldo() {
        System.out.println("\nSeu saldo é de R$" + this.saldo);
    }

    public void sacar(double valor) {
        if (this.saldo >= valor) {
            this.saldo = this.saldo - valor;
            System.out.println("\nSaque realizado com sucesso!");
        } else {
            System.out.println("\nVocê não pode sacar mais que o seu saldo atual!");
        }
        System.out.println("Seu saldo é de R$" + this.saldo);
    }

    public void depositar(double valor) {
        this.saldo = this.saldo + valor;
        System.out.println("\nDeposito realizado com sucesso!");
        System.out.println("Seu saldo é de R$" + this.saldo);
    }


}
