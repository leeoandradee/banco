package br.com.itau.cliente;

import br.com.itau.conta.Conta;

import java.util.Scanner;

public class Cliente {

    private String nome;
    private int idade;
    private Conta conta = null;

    public Cliente(String nome, int idade) {
        this.nome = nome;
        this.idade = idade;
    }

    public static boolean clienteEhMaiorIdade(int idade) {
        return idade > 18 ? true : false;
    }

    public static Cliente cadastrarCliente() {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite seu nome:");
        String nome = scanner.nextLine();

        System.out.println("Digite sua idade:");
        String idade = scanner.nextLine();

        if (clienteEhMaiorIdade(Integer.parseInt(idade))) {
            System.out.println("Cliente cadastrado com sucesso!");
            return new Cliente(nome, Integer.parseInt(idade));
        } else {
            System.out.println("Cliente não apto a criar a conta!");
            return null;
        }
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }
}
