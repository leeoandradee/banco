package br.com.itau.main;

import br.com.itau.cliente.Cliente;
import br.com.itau.conta.Conta;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Cliente cliente = Cliente.cadastrarCliente();
        if (cliente != null) {
            Conta conta = new Conta();
            cliente.setConta(conta);

            cliente.getConta().getSaldo();
            cliente.getConta().depositar(100.0);
            cliente.getConta().sacar(100.0);
            cliente.getConta().sacar(100.0);
        }
    }
}
